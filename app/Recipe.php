<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = ['title', 'description', 'kcal', 'fat', 'protein', 'carbohydrates', 'user_id', 'imageUrl', 'category'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function ingredients() {
        return $this->hasMany('App\Ingredient');
    }

    public function instructions() {
        return $this->hasMany('App\Instruction');
    }

    public function tags() {
        return $this->hasMany('App\Tag');
    }
}
