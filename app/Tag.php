<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['tag', 'recipe_id'];

    public function recipe() {
        return $this->belongsTo(Recipe::class);
    }
}
