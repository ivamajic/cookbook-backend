<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = ['ingredient', 'recipe_id'];

    public function recipe() {
        return $this->belongsTo(Recipe::class);
    }
}
