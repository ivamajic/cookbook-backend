<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Ingredient;
use App\Instruction;
use App\Tag;

class Recipe extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'kcal' => (int) $this->kcal,
            'fat' => (int) $this->fat,
            'protein' => (int) $this->protein,
            'carbohydrates' => (int) $this->carbohydrates,
            'user_id' => (int) $this->user_id,
            'imageUrl' => $this->imageUrl,
            'category' => $this->category,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'tags' => Tag::orderBy('created_at', 'desc')->where('recipe_id', $this->id)->get(),
            'ingredients' => Ingredient::orderBy('created_at', 'desc')->where('recipe_id', $this->id)->get(),
            'instructions' => Instruction::orderBy('created_at', 'desc')->where('recipe_id', $this->id)->get(),
        ];
    }
}
