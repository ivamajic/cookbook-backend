<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Recipe;
use App\Http\Resources\Recipe as RecipeResource;
use App\Http\Resources\RecipeCollection;
use Auth;
use App\Ingredient;
use App\Instruction;
use App\Tag;
use App\User;

class RecipeController extends Controller
{
    public function getAll() {
        $recipes = Recipe::orderBy('created_at', 'desc')->get();
        foreach($recipes as $recipe) {
            $recipe->ingredients = $recipe->ingredients()->get();
            $recipe->instructions = $recipe->instructions()->get();
            $recipe->tags = $recipe->tags()->get();
            $recipe->user = $recipe->user()->get();
        }        
        return $recipes;
    }

    public function getById($id) {
        $recipe = Recipe::findOrFail($id);
        $recipe->ingredients = $recipe->ingredients()->get();
        $recipe->instructions = $recipe->instructions()->get();
        $recipe->tags = $recipe->tags()->get();
        $recipe->user = $recipe->user()->get();
        return $recipe;
    }

    public function manage($id, $role) {
        if ($role === "admin") {
            $recipes = Recipe::orderBy('created_at', 'desc')->get();
            foreach($recipes as $recipe) {
                $recipe->ingredients = $recipe->ingredients()->get();
                $recipe->instructions = $recipe->instructions()->get();
                $recipe->tags = $recipe->tags()->get();
                $recipe->user = $recipe->user()->get();
            }        
            return $recipes;
        } else {
            $recipes = Recipe::where('user_id', $id)->get();
            foreach($recipes as $recipe) {
                $recipe->ingredients = $recipe->ingredients()->get();
                $recipe->instructions = $recipe->instructions()->get();
                $recipe->tags = $recipe->tags()->get();
                $recipe->user = $recipe->user()->get();
            }        
            return $recipes;
        }
    }

    public function create(Request $request) {
        $recipe = new Recipe;
        $recipe->title = $request->title;
        $recipe->description = $request->description;
        $recipe->imageUrl = $request->imageUrl;
        $recipe->kcal = $request->kcal;
        $recipe->protein = $request->protein;
        $recipe->fat = $request->fat;
        $recipe->carbohydrates = $request->carbohydrates;
        $recipe->category = $request->category;
        $recipe->user_id = $request->user_id;
        if ($recipe->save()) {
            foreach($request->ingredients as $ing) {
                $ingredient = new Ingredient;
                $ingredient->ingredient = $ing;
                $ingredient->recipe_id = $recipe->id;
                $ingredient->save();
            }
            foreach($request->instructions as $ins) {
                $instruction = new Instruction;
                $instruction->instruction = $ins;
                $instruction->recipe_id = $recipe->id;
                $instruction->save();
            }    
            foreach($request->tags as $tg) {
                $tag = new Tag;
                $tag->tag = $tg;
                $tag->recipe_id = $recipe->id;
                $tag->save();
            }
            $recipe->ingredients = $request->ingredients;
            $recipe->instructions = $request->instructions;
            $recipe->tags = $request->tags;
            return $recipe;
        }
        return "error";        
    }

    public function update($id, Request $request) {
        $recipe = Recipe::findOrFail($id);
        $recipe->title = $request->title;
        $recipe->description = $request->description;
        $recipe->imageUrl = $request->imageUrl;
        $recipe->kcal = $request->kcal;
        $recipe->protein = $request->protein;
        $recipe->fat = $request->fat;
        $recipe->carbohydrates = $request->carbohydrates;
        $recipe->category = $request->category;
        if ($recipe->save()) {
            $recipe->ingredients()->delete();
            $recipe->instructions()->delete();
            $recipe->tags()->delete();
            foreach($request->ingredients as $ing) {
                $ingredient = new Ingredient;
                $ingredient->ingredient = $ing;
                $ingredient->recipe_id = $recipe->id;
                $ingredient->save();
            }
            foreach($request->instructions as $ins) {
                $instruction = new Instruction;
                $instruction->instruction = $ins;
                $instruction->recipe_id = $recipe->id;
                $instruction->save();
            }    
            foreach($request->tags as $tg) {
                $tag = new Tag;
                $tag->tag = $tg;
                $tag->recipe_id = $recipe->id;
                $tag->save();
            }
            $recipe->ingredients = $request->ingredients;
            $recipe->instructions = $request->instructions;
            $recipe->tags = $request->tags;
            return $recipe;
        }
        return "error";    
    }

    public function delete($id) {
        $recipe = Recipe::findOrFail($id);
        $recipe->ingredients()->delete();
        $recipe->instructions()->delete();
        $recipe->tags()->delete();
        if ($recipe->delete()) {
            return "Successfully deleted";
        }
        return "Not deleted";
    }
}
