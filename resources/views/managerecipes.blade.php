@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>All recipes</title>
        <script src="https://kit.fontawesome.com/93b52026cc.js"></script>
        <link href="{{ asset('css/recipes.css') }}" rel="stylesheet" type="text/css" >
    </head>
    <body>
        @if(count($recipes) > 0)
            @foreach ($recipes as $recipe)
            <div class="recipe">
                <a class="recipe__title"  href="{{ route('recipe', $recipe->id) }}">
                    {{$recipe->title}}
                </a>
                <a class="recipe__image" href="{{ route('recipe', $recipe->id) }}">
                    <img src="{{$recipe->imageUrl}}" />
                </a>
                <div class="recipe__description">
                    {{$recipe->description}}
                </div>
                <div class="recipe__buttons">
                    <a class="recipe__button" href="{{ route('editrecipe', $recipe->id) }}"><i class="fas fa-edit"></i></a>
                    
                    <form method="POST" action="{{ route('deleterecipe', $recipe->id) }}">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="recipe__button" type="submit"><i class="fas fa-trash-alt"></i></button>
                    </form>
                </div>
            </div>
            @endforeach
        @endif
    </body>
</html>
@endsection