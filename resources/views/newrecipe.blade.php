@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>New recipe</title>
        <script src="https://kit.fontawesome.com/93b52026cc.js"></script>
        <link href="{{ asset('css/recipeform.css') }}" rel="stylesheet" type="text/css" >
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function() {
            let i = 1;
            let j = 1;
            let k = 1;
            $('#addingredients').click(function(){
                i++;
                $('#ingredients').append('<div class="input-button" id="ingredient'+i+'"><input class="form-control" type="text" name="ingredients[]" autocomplete="off" required> <button type="button" name="removeingredients" class="btn__removeingredients" id="'+i+'"><i class="fas fa-trash-alt"></i></button></div>');
            });

            $(document).on('click', '.btn__removeingredients', function() {
                let button_id = $(this).attr("id");
                $('#ingredient'+button_id+'').remove();
            });

            $('#addinstructions').click(function(){
                j++;
                $('#instructions').append('<div class="input-button" id="instruction'+j+'"><input class="form-control" type="text" name="instructions[]" autocomplete="off" required> <button type="button" name="removeinstructions" class="btn__removeinstructions" id="'+j+'"><i class="fas fa-trash-alt"></i></button></div>');
            });

            $(document).on('click', '.btn__removeinstructions', function() {
                let button_id = $(this).attr("id");
                $('#instruction'+button_id+'').remove();
            });

            $('#addtags').click(function(){
                i++;
                $('#tags').append('<div class="input-button" id="tag'+k+'"><input class="form-control" type="text" name="tags[]" autocomplete="off" required> <button type="button" name="removetags" class="btn__removetags" id="'+k+'"><i class="fas fa-trash-alt"></i></button></div>');
            });

            $(document).on('click', '.btn__removetags', function() {
                let button_id = $(this).attr("id");
                $('#tag'+button_id+'').remove();
            });
        });
        </script>
    </head>
    <body>
        <div class="form-container"> 
            <div class="card">
                <div class="card-header">New recipe</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('postrecipe') }}">
                    @csrf
                        <div class="form-group row">
                            <label for="title">Title: </label>
                            <input class="form-control" type="text" name="title" id="title" autocomplete="off" required>
                        </div>
                        <div class="form-group row">
                            <label for="description">Description: </label>
                            <textarea class="form-control" rows=5 name="description" id="description" autocomplete="off" required></textarea>
                        </div>
                        <div class="form-group row">
                            <label for="imageUrl">Image URL: </label>
                            <input class="form-control" type="text" name="imageUrl" id="imageUrl" autocomplete="off" required>
                        </div>
                        <div class="form-group row">
                            <label for="category">Category: </label>
                            <select name="category" id="category" required>
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch">Lunch</option>
                                <option value="dinner">Dinner</option>
                                <option value="dessert">Dessert</option>
                                <option value="other">Other</option>
                            </select> 
                        </div>
                        <div class="form-group row">
                            <table class="recipe__nutrition">
                                <tr>
                                    <th><label for="kcal">Energy (kcal): </label></th>
                                    <th><label for="fat">Fats (g): </label></th>
                                    <th><label for="protein">Proteins (g): </label></th>
                                    <th><label for="carbohydrates">Carbs (g): </label></th>
                                </tr>
                                <tr>
                                    <td><input class="form-control" type="number" name="kcal" id="kcal" autocomplete="off" required></td>
                                    <td><input class="form-control" type="number" name="fat" id="fat" autocomplete="off" required></td>
                                    <td><input class="form-control" type="number" name="protein" id="protein" autocomplete="off" required></td>
                                    <td><input class="form-control" type="number" name="carbohydrates" id="carbohydrates" autocomplete="off" required></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group row" id="ingredients">
                            <label for="ingredients">Ingredients: </label>
                            <div class="input-button">
                                <input class="form-control" type="text" name="ingredients[]" autocomplete="off" required>
                                <button type="button" name="addingredients" id="addingredients"><i class="fas fa-plus-circle"></i></button>
                            </div>
                        </div>
                        <div class="form-group row" id="instructions">
                            <label for="instructions">Instructions: </label>
                            <div class="input-button">
                                <input class="form-control" type="text" name="instructions[]" autocomplete="off" required>
                                <button type="button" name="addinstructions" id="addinstructions"><i class="fas fa-plus-circle"></i></button>
                            </div>
                        </div>
                        <div class="form-group row" id="tags">
                            <label for="tags">Tags: </label>
                            <div class="input-button">
                                <input class="form-control" type="text" name="tags[]" autocomplete="off" required>
                                <button type="button" name="addtags" id="addtags"><i class="fas fa-plus-circle"></i></button>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn-submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
@endsection
