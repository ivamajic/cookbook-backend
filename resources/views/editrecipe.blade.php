@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>New recipe</title>
        <script src="https://kit.fontawesome.com/93b52026cc.js"></script>
        <link href="{{ asset('css/recipeform.css') }}" rel="stylesheet" type="text/css" >
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            let ingredients = @json($recipe->ingredients);
            let instructions = @json($recipe->instructions);
            let tags = @json($recipe->tags);
            let i = ingredients.length + instructions.length + tags.length;
                $('#addingredients').click(function(){
                    i++;
                    $('#ingredients').append('<div class="form-group row"><div class="input-button" id="row'+i+'"><input class="form-control" type="text" name="ingredients[]" required> <button type="button" name="removeingredients" class="btn__remove" id="'+i+'"><i class="fas fa-trash-alt"></i></button></div></div>');
                });

                $(document).on('click', '.btn__remove', function() {
                    let button_id = $(this).attr("id");
                    $('#row'+button_id+'').remove();
                    $('#'+button_id).remove();
                });

                $('#addinstructions').click(function(){
                    i++;
                    $('#instructions').append('<div class="form-group row"><div class="input-button" id="row'+i+'"><input class="form-control" type="text" name="instructions[]" required> <button type="button" name="removeinstructions" class="btn__remove" id="'+i+'"><i class="fas fa-trash-alt"></i></button></div></div>');
                });
                
                $('#addtags').click(function(){
                    i++;
                    $('#tags').append('<div class="form-group row"><div class="input-button" id="row'+i+'"><input class="form-control" type="text" name="tags[]" required> <button type="button" name="removetags" class="btn__remove" id="'+i+'"><i class="fas fa-trash-alt"></i></button></div></div>');
                });

            });
        </script>
    </head>
    <body>
        <div class="form-container"> 
            <div class="card">
                <div class="card-header">Edit recipe {{$recipe->title}}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('saverecipe', $recipe->id) }}">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                        <div class="form-group row">
                            <label for="title">Title: </label>
                            <input class="form-control" type="text" value="{{$recipe->title}}" name="title" id="title" required>
                        </div>
                        <div class="form-group row">
                            <label for="description">Description: </label>
                            <textarea class="form-control" rows=5 name="description" id="description" required>{{$recipe->description}}</textarea>
                        </div>
                        <div class="form-group row">
                            <label for="imageUrl">Image URL: </label>
                            <input class="form-control" type="text" name="imageUrl" id="imageUrl"  value="{{$recipe->imageUrl}}" required>
                        </div>
                        <div class="form-group row">
                            <img style="width: 100%; margin: 20px 0;" src="{{$recipe->imageUrl}}" />
                        </div>
                        <div class="form-group row">
                            <label for="category">Category: </label>
                            <select name="category" id="category" required>
                                <option value="breakfast">Breakfast</option>
                                <option value="lunch">Lunch</option>
                                <option value="dinner">Dinner</option>
                                <option value="dessert">Dessert</option>
                                <option value="other">Other</option>
                            </select> 
                        </div>
                        <div class="form-group row">
                            <table class="recipe__nutrition">
                                <tr>
                                    <th><label for="kcal">Energy (kcal): </label></th>
                                    <th><label for="fat">Fats (g): </label></th>
                                    <th><label for="protein">Proteins (g): </label></th>
                                    <th><label for="carbohydrates">Carbs (g): </label></th>
                                </tr>
                                <tr>
                                    <td> <input class="form-control" type="number" name="kcal" id="kcal" value="{{$recipe->kcal}}" required></td>
                                    <td><input class="form-control" type="number" name="fat" id="fat" value="{{$recipe->fat}}" required></td>
                                    <td><input class="form-control" type="number" name="protein" id="protein" value="{{$recipe->protein}}" required></td>
                                    <td><input class="form-control" type="number" name="carbohydrates" id="carbohydrates" value="{{$recipe->carbohydrates}}" required></td>
                                </tr>
                            </table>
                        </div>
                        @if(count($recipe->ingredients) > 0)   
                            <div id="ingredients">
                                <label for="ingredients" style="margin-left: -15px">Ingredients: </label>
                                @foreach ($recipe->ingredients as $ingredient)
                                    <div class="form-group row">
                                        <div class="input-button">
                                            <input value="{{$ingredient->ingredient}}" class="form-control" type="text" name="ingredients[]" id="row{{$loop->index}}" required>
                                            <button type="button" name="removeingredients" class="btn__remove" id="{{$loop->index}}"><i class="fas fa-trash-alt"></i></button>
                                        </div>
                                    </div>
                                @endforeach 
                            </div>
                            <div class="add-btn">
                                <button type="button" name="addingredients" id="addingredients">Add more ingredients</button>
                            </div>
                        @endif
                        @if(count($recipe->instructions) > 0)   
                            <label for="instructions" style="margin-left: -15px">Instructions: </label>
                            <div id="instructions">
                                @foreach ($recipe->instructions as $instruction)
                                    <div class="form-group row">
                                        <div class="input-button">
                                            <input value="{{$instruction->instruction}}" class="form-control" type="text" name="instructions[]" id="row{{count($recipe->ingredients) + $loop->index}}" required>
                                            <button type="button" name="removeinstructions" class="btn__remove" id="{{count($recipe->ingredients) + $loop->index}}"><i class="fas fa-trash-alt"></i></button>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="add-btn">
                                <button type="button" name="addinstructions" id="addinstructions">Add more instructions</button>
                            </div>
                        @endif
                        @if(count($recipe->tags) > 0)   
                            <label for="tags" style="margin-left: -15px">Tags: </label>
                            <div id="tags">
                                @foreach ($recipe->tags as $tag)
                                    <div class="form-group row">
                                        <div class="input-button">
                                            <input value="{{$tag->tag}}" class="form-control" type="text" name="tags[]" id="row{{count($recipe->ingredients) + count($recipe->instructions) + $loop->index}}" required>
                                            <button type="button" name="removetags" class="btn__remove" id="{{count($recipe->ingredients) + count($recipe->instructions) + $loop->index}}"><i class="fas fa-trash-alt"></i></button>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="add-btn">
                                <button type="button" name="addtags" id="addtags">Add more tags</button>
                            </div>
                        @endif
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn-submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
@endsection
