@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Recipe</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="{{ asset('css/recipe.css') }}" rel="stylesheet" type="text/css" >
        <style>
            .container {                
                width: 100vw;
                height: 100vh;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .justify-content-center {
                width: 800px;
            }
            .link {
                color: black !important;
                text-decoration: underline;
            }
            .link:hover {
                color: #007bff !important;
            }
        </style>
    </head>
    <body>
        @if($recipe)
            <div class="recipe">
                <div class="recipe__title">
                    {{$recipe->title}}
                </div>
                <div class="recipe__category">
                    Category: {{$recipe->category}}
                </div>
                @if(count($recipe->tags) > 0)
                    <div class="recipe__category">
                        Tags:
                        @foreach ($recipe->tags as $tag)
                            {{$tag->tag}}
                        @endforeach
                    </div>
                @endif
                <div class="recipe__image">
                    <img src="{{$recipe->imageUrl}}" />
                </div>
                <div class="recipe__description">
                    {{$recipe->description}}
                </div>
                <table class="recipe__nutrition">
                    <tr>
                        <th>Energy (kcal)</th>
                        <th>Fats (g)</th>
                        <th>Protein (g)</th>
                        <th>Carbohydrates (g)</th>
                    </tr>
                    <tr>
                        <td>{{$recipe->kcal}}</td>
                        <td>{{$recipe->fat}}</td>
                        <td>{{$recipe->protein}}</td>
                        <td>{{$recipe->carbohydrates}}</td>
                    </tr>   
                </table>
            </div>
        @endif  
        <div class="recipe__method">
            <ul>     
                @if(count($recipe->ingredients) > 0) 
                    <div class="method__label">Ingredients:</div>
                    @foreach ($recipe->ingredients as $ingredient)
                    <li>
                        {{$ingredient->ingredient}}
                    </li>
                    @endforeach
                @endif
            </ul>       
            <ul> 
                @if(count($recipe->instructions) > 0)
                    <div class="method__label">Instructions:</div>
                    @foreach ($recipe->instructions as $instruction)
                    <li>
                        {{$instruction->instruction}}
                    </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </body>
</html>
@endsection