<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Recipe;
use App\Ingredient;
use App\Instruction;
use App\Tag;
use Illuminate\Http\Request;

Auth::routes();



Route::get('/', function () {
    $recipes = Recipe::orderBy('created_at', 'desc')->get();
    return view('recipes', [
        'recipes' => $recipes
    ]);
})->name('recipes');

/*Route::get('/recipes', function () {
    $recipes = Recipe::orderBy('created_at', 'desc')->get();
    return view('recipes', [
        'recipes' => $recipes
    ]);
})->name('recipes');

Route::get('/managerecipes', function () {
    if(!Auth::check()) {
        return redirect('login'); 
    }
    if (Auth::user()->getRole() === 'admin') {
        $recipes = Recipe::orderBy('created_at', 'desc')->get();
    }
    else {
        $recipes = Auth::user()->recipes;
    }
    return view('managerecipes', [
        'recipes' => $recipes
    ]);
})->name('managerecipes');

Route::get('/recipe/{recipe_id?}', function ($recipe_id = null) {
    $recipe = Recipe::find($recipe_id);
    return view('recipe', [
        'recipe' => $recipe
    ]);
})->name('recipe');


Route::get('/newrecipe', function () {
    if(!Auth::check()) {
        return redirect('login'); 
    }
    return view('newrecipe');
})->name('newrecipe');

Route::get('/editrecipe/{recipe_id?}', function ($recipe_id = null) {
    if(!Auth::check()) {
        return redirect('login'); 
    }
    $recipe = Recipe::find($recipe_id);
    if (Auth::user()->getRole() !== 'admin' && $recipe->user_id !== Auth::user()->getId()) {
        return redirect('recipes'); 
    }
    return view('editrecipe', [
        'recipe' => $recipe
    ]);
})->name('editrecipe');

Route::put('/saverecipe/{recipe_id?}', function (Request $request, $recipe_id = null) {
    if(!Auth::check()) {
        return redirect('login'); 
    }
    $recipe = Recipe::find($recipe_id);
    if (Auth::user()->getId() !== $recipe->user_id) {
        return redirect('recipes');
    }
    $recipe->title = $request->title;
    $recipe->description = $request->description;
    $recipe->imageUrl = $request->imageUrl;
    $recipe->kcal = $request->kcal;
    $recipe->protein = $request->protein;
    $recipe->fat = $request->fat;
    $recipe->carbohydrates = $request->carbohydrates;
    $recipe->category = $request->category;
    $saved = $recipe->save();
    if ($saved) {
        foreach($recipe->ingredients as $ing) {
            Ingredient::findOrFail($ing->id)->delete();
        }
        foreach($recipe->instructions as $ins) {
            Instruction::findOrFail($ins->id)->delete();
        }
        foreach($request->ingredients as $ing) {
            $ingredient = new Ingredient;
            $ingredient->ingredient = $ing;
            $ingredient->recipe_id = $recipe->id;
            $ingredient->save();
        }
        foreach($request->instructions as $ins) {
            $instruction = new Instruction;
            $instruction->instruction = $ins;
            $instruction->recipe_id = $recipe->id;
            $instruction->save();
        }    
    }
    return redirect('/managerecipes');
})->name('saverecipe');

Route::post('/postrecipe', function (Request $request) {
    if(!Auth::check()) {
        return redirect('login'); 
    }
    $recipe = new Recipe;
    $recipe->title = $request->title;
    $recipe->description = $request->description;
    $recipe->imageUrl = $request->imageUrl;
    $recipe->kcal = $request->kcal;
    $recipe->protein = $request->protein;
    $recipe->fat = $request->fat;
    $recipe->carbohydrates = $request->carbohydrates;
    $recipe->category = $request->category;
    $recipe->user_id = Auth::user()->getId();
    $recipe->save();
    foreach($request->ingredients as $ing) {
        $ingredient = new Ingredient;
        $ingredient->ingredient = $ing;
        $ingredient->recipe_id = $recipe->id;
        $ingredient->save();
    }
    foreach($request->instructions as $ins) {
        $instruction = new Instruction;
        $instruction->instruction = $ins;
        $instruction->recipe_id = $recipe->id;
        $instruction->save();
    }    
    foreach($request->tags as $tg) {
        $tag = new Tag;
        $tag->tag = $tg;
        $tag->recipe_id = $recipe->id;
        $tag->save();
    } 
    return redirect()->route('recipe', ['id' => $recipe->id]);
})->name('postrecipe');

Route::delete('/deleterecipe/{recipe_id?}', function (Request $request, $recipe_id = null) {
    if(!Auth::check()) {
        return redirect('login'); 
    }
    $recipe = Recipe::find($recipe_id);
    $ingredients = $recipe->ingredients;
    $instructions = $recipe->instructions;
    if (Auth::user()->getId() !== $recipe->user_id) {
        return redirect('recipes');
    }
    $deleted = Recipe::findOrFail($recipe_id)->delete();
    if ($deleted) {
        foreach($ingredients as $ing) {
            Ingredient::findOrFail($ing->id)->delete();
        }
        foreach($instructions as $ins) {
            Instruction::findOrFail($ins->id)->delete();
        }
    }
    return redirect('managerecipes');
})->name('deleterecipe');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/