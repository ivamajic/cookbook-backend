<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});


Route::group([
    'prefix' => 'recipes'
], function () {
    Route::get('', 'RecipeController@getAll');
    Route::get('manage/{id}/{role}', 'RecipeController@manage');
    Route::get('{id}', 'RecipeController@getById');
    Route::post('', 'RecipeController@create');
    Route::put('{id}', 'RecipeController@update');
    Route::delete('{id}', 'RecipeController@delete');
});


