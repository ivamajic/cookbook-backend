@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>All recipes</title>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" >
    </head>
    <body>
        @if(count($recipes) > 0)
            @foreach ($recipes as $recipe)
            <div class="recipe">
                <a class="recipe__title"  href="{{ route('recipe', $recipe->id) }}">
                    {{$recipe->title}}
                </a>
                <a class="recipe__image" href="{{ route('recipe', $recipe->id) }}">
                    <img src="{{$recipe->imageUrl}}" />
                </a>
                <div class="recipe__description">
                    {{$recipe->description}}
                </div>
            </div>
            @endforeach
        @endif
    </body>
</html>
@endsection